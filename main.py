import requests
import locale
import datetime

def get_openweathermap(api_key,city_id):
    url="https://api.openweathermap.org/data/2.5/weather?id=" +city_id + "&lang=ru&units=metric&appid=" + api_key
    response = requests.post(url)
    if response.status_code == 200:
        return response.text
    else:
        return 0


def get_date(full_data):
    loc = locale.getlocale()
    locale.setlocale(locale.LC_ALL, ('RU', 'UTF8'))
    format = '%d %b %Y, %H:%M:%S'
    date = datetime.datetime.strptime(full_data, format)
    locale.setlocale(locale.LC_ALL, loc)
    return date

def get_heat_data(pos):
    post_data = {"pos": pos}
    url = "http://81.30.218.195:81/tekl/data.php"
    response = requests.post(url, data=post_data)
    if response.status_code == 200:
        return response.text
    else:
        return 0

def parsing_heat_data(response, pos):
    if(response != 0):
        tmp_list = []
        slice_start_pos = 0
        slice_end_pos = 0
        i = 0
        for char in response:
            if(char == ">"):        #Get data between tags (strong> data </strong)
                slice_start_pos = i
            if(char == "<"):
                slice_end_pos = i
                if(slice_end_pos-slice_start_pos > 1):
                    tmp_string = response[slice_start_pos+1:slice_end_pos].strip()
                    if(tmp_string != "\\r\\n"):
                        tmp_list.append(tmp_string)
            i = i+1

        res_data = {
                    "Adress": tmp_list[0],
                    "pos": pos,
                    "Date": get_date(tmp_list[3]),
                    "T1": float(tmp_list[5]),
                    "T2": float(tmp_list[8]),
                    "G1": float(tmp_list[11]),
                    "G2": float(tmp_list[14]),
                    "M1": float(tmp_list[17]),
                    "M2": float(tmp_list[20]),
                    "Q": float(tmp_list[23]),
                    "W": float(tmp_list[26]),
                    "BHP": float(tmp_list[29])
                   }

        print(res_data)
        return res_data
    else:
        return 0

def main():
    print("start process")
    houses = [5231, 6588, 2642] #Калинина 12, Блюхера 14, Блюхера 18
    for house in houses:
        parsing_heat_data(get_heat_data(house), house)
        print("===================")

    print(get_openweathermap("0ef8e5ce1cccbcde4ccb4e4f0a55c72a","479561"))

main()